#ifndef TP4_TABLEAU_HPP
#define TP4_TABLEAU_HPP

const int T_MAX_SIZE = 100;


void afficherTableau(int tableau[], int size);

void initialiserTableau(int tableau[], int size, int default_value);

void remplirTableau(int tableau[], int size);

int somme(int tableau[], int size);

double moyenne(int tableau[], int size);

void carre(int tableau[], int resultat[], int size);


#endif
