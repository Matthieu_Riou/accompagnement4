#ifndef TP4_STRING_HPP
#define TP4_STRING_HPP

#include <string>


int count(std::string s, char c);

std::string replace(std::string s, char c, char r);

std::string inverse(std::string s);

std::string substring(std::string s, int pos_start, int pos_fin);

bool estPalindrome(std::string s);

bool estPalindrome_recursif(std::string s);


#endif
